require('./index.css');

var _mm     = require('util/mm.js');

//导航

var header = {
    init : function(){
        this.onLoad();
        this.bindEvent();
    },
    onLoad : function(){
        var keyword = _mm.getUrlParam('keyword');
        // keyword存在，则回填输入框
        if(keyword){
            $('#search-input').val(keyword);
        };
    },
    bindEvent : function(){
        var _this=this;
        $('#search-btn').click(function(){
            _this.searchSubmit();
        });
        $('#search-input').keyup(function(e){
            if(e.keyCode===13){
                _this.searchSubmit();
            }
        });
    },
    // 搜索
    searchSubmit:function(){
        var keyword=$.trim($('#search-input').val());
        if(keyword){
            window.location.href='./list.html?keyword='+keyword;
        }
        else{
            _mm.goHome();
        }
    }
  
    
};

header.init();