
// module.exports={
//     entry:'./src/page/index/index.js',
//     output:{
//         //path:'./dist',
//         path:path.join(__dirname,'dist'),
//         filename:'app.js'
//     }
// }
// 
var path=require('path');
var ExtractTextPlugin =require("extract-text-webpack-plugin");
var webpack=require('webpack');
var HtmlWebpackPlugin=require('html-webpack-plugin');
var getHtmlConfig=function(name,title){
    return {
        title:title,
        template:'./src/view/'+name+'.html',
        filename:'view/'+name+'.html',
        inject:true,
        hash:true,
        chunks:['common',name]
    };
};
var config={
    // externals:{
    //     '$':'window.jQuery',
    //     'jquery':'window.jQuery'
    // },
    entry:{
        'common' :['./src/page/common/index.js'],
        'index' :['./src/page/index/index.js'],
        'list' :['./src/page/list/index.js'],
        'user-login' :['./src/page/user-login/index.js'],
        'result' :['./src/page/result/index.js'],
        'user-register' :['./src/page/user-register/index.js'],
        'user-pass-reset':['./src/page/user-pass-reset/index.js'],
        'user-center-update':['./src/page/user-center-update/index.js'],
        'user-center':['./src/page/user-center/index.js'],
        'user-pass-update':['./src/page/user-pass-update/index.js'],
        'detail':['./src/page/detail/index.js'],
        'cart':['./src/page/cart/index.js'],
        'order-confirm'     : ["./src/page/order-confirm/index.js"],
        'order-list'        : ["./src/page/order-list/index.js"],
        'order-detail'      : ["./src/page/order-detail/index.js"],
        'payment' : ["./src/page/payment/index.js"],
    },
    output:{
        path:path.join(__dirname,'dist'),
        publicPath:'/dist/',
        filename:'js/[name].js'
    },

    module:{
        rules:[
            {
                test:/\.css$/,
                //注意：这里还需要更改一下
                use:ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: "css-loader"
                })
            },
            {
                test: /\.(eot|woff|woff2|ttf|png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            },
            { 
                test: /\.string$/, 
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: [':data-src']
                    }
                }
            }
           
        ]

    },
    resolve:{
        alias:{
                'util':path.resolve(__dirname,'src/util'),
                'node_modules':path.resolve(__dirname,'node_modules'),
                'page':path.resolve(__dirname,'src/page'),
                'service':path.resolve(__dirname,'src/service'),
                'image':path.resolve(__dirname,'src/image')
        }
    },
    

    plugins:[
        new webpack.ProvidePlugin({
          jQuery: "jquery",
          $: "jquery"
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name : 'common',
            filename :'js/base.js'
        }),
        new ExtractTextPlugin("css/[name].css"),
        new HtmlWebpackPlugin(getHtmlConfig('index','首页')),
        new HtmlWebpackPlugin(getHtmlConfig('user-login','登录页')),
        new HtmlWebpackPlugin(getHtmlConfig('test','测试')),
        new HtmlWebpackPlugin(getHtmlConfig('result','测试结果')),
        new HtmlWebpackPlugin(getHtmlConfig('user-register','用户注册')),
        new HtmlWebpackPlugin(getHtmlConfig('user-pass-reset','用户重置密码')),
        new HtmlWebpackPlugin(getHtmlConfig('user-center','个人中心')),
        new HtmlWebpackPlugin(getHtmlConfig('user-center-update','修改跟人信息')),
        new HtmlWebpackPlugin(getHtmlConfig('user-pass-update','修改密码')),
        new HtmlWebpackPlugin(getHtmlConfig('list','商品列表')),
        new HtmlWebpackPlugin(getHtmlConfig('detail','商品详情')),
        new HtmlWebpackPlugin(getHtmlConfig('cart','购物车')),
        new HtmlWebpackPlugin(getHtmlConfig('order-confirm', '订单确认')),
        new HtmlWebpackPlugin(getHtmlConfig('order-list', '订单列表')),
        new HtmlWebpackPlugin(getHtmlConfig('order-detail', '订单详情')),
        new HtmlWebpackPlugin(getHtmlConfig('payment', '支付')),

    ]
};
module.exports=config;